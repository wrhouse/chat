import aiohttp.web

from chat.api.ws.order.shop import routes as shop
from chat.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/shop', shop)
