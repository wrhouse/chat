import aiohttp.web

from chat.api.ws.general.shop.views import websocket_receive


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix, websocket_receive)
