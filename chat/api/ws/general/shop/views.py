from typing import Optional

import aiohttp
import aiohttp.web
import ujson as json
from sqlalchemy import exists, select, literal

from chat.db.general.shop.channel import ChannelModel
from chat.utils.channel import Channel


def get_channel_key(channel_id: int) -> str:
    return f'channel:{channel_id}'


async def channel_is_exist(app: aiohttp.web.Application,
                           channel_id: int) -> bool:
    async with app['db'].acquire() as conn:
        query = (select([literal(True)])
                 .where(exists()
                        .where(ChannelModel.channel_id == channel_id)))
        result = await conn.scalar(query)
        return bool(result)


async def subscribe(app: aiohttp.web.Application,
                    ws: aiohttp.web.WebSocketResponse,
                    channel_id: int) -> None:
    channel_exist = await channel_is_exist(app, channel_id)
    if not channel_exist:
        return

    channel_key = get_channel_key(channel_id)
    chat_channels = app['channels']['general']['shop']
    if channel_key not in chat_channels:
        chat_channels[channel_key] = Channel(channel_key, app['redis'])
    if not chat_channels[channel_key].is_running():
        chat_channels[channel_key].start()
    chat_channels[channel_key].add_connection(ws)


async def unsubscribe(app: aiohttp.web.Application,
                      ws: aiohttp.web.WebSocketResponse,
                      channel_id: Optional[int] = None) -> None:
    chat_channels = app['channels']['general']['shop']

    channel_keys = []
    if channel_id is None:
        channel_keys = chat_channels.keys()
    else:
        channel_exist = await channel_is_exist(app, channel_id)
        if not channel_exist:
            return
        channel_keys.append(get_channel_key(channel_id))

    for channel_key in channel_keys:
        if channel_key not in chat_channels:
            return

        chat_channels[channel_key].remove_connection(ws)
        if chat_channels[channel_key].is_empty():
            await chat_channels[channel_key].stop()


async def websocket_receive(request: aiohttp.web.Request):
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            data = json.loads(msg.data)
            command = data.get('command')
            channel_id = data.get('channel_id')
            if type(channel_id) == int:
                if command == 'subscribe':
                    await subscribe(request.app, ws, channel_id)
                elif command == 'unsubscribe':
                    await unsubscribe(request.app, ws, channel_id)

    await unsubscribe(request.app, ws)
    return ws
