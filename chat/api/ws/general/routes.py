import aiohttp.web

from chat.api.ws.general.forum import routes as forum
from chat.api.ws.general.shop import routes as shop
from chat.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/forum', forum)
    include_urls(app, prefix + '/shop', shop)
