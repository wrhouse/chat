import aiohttp.web

from chat.api.ws.general import routes as general
from chat.api.ws.order import routes as order
from chat.api.ws.support import routes as support
from chat.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/general', general)
    include_urls(app, prefix + '/order', order)
    include_urls(app, prefix + '/support', support)
