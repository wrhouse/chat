import aiohttp.web

from chat.api.rest import routes as rest
from chat.api.ws import routes as ws
from chat.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/rest', rest)
    include_urls(app, prefix + '/ws', ws)
