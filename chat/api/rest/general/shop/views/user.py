import aiohttp.web

from chat.decorators import authorized, check_permissions


@authorized('admin')
async def get_users(request: aiohttp.web.Request):
    """
    ---
    summary: Получить список пользователей чата
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: is_online
          in: query
          type: integer
          required: false
          minimum: 0
          maximum: 1
          default: 0
          description: Флаг, фильтрующий пользователей по статусу онлайна (0 - возвращаются все пользователи
                       чата (по умолчанию), 1 - возвращаются только пользователи онлайн).
        - name: is_banned
          in: query
          type: integer
          required: false
          minimum: 0
          maximum: 1
          default 0
          description: Флаг, фильтрующий пользователей по статусу блокировки (0 - возвращаются все пользователи
                       (по умолчанию), 1 - возвращаются только заблокированные пользователи).
        - name: count
          in: query
          type: integer
          required: false
          minimum: 1
          maximum: 200
          default: 20
          description: Количество пользователей, информацию о которых требуется вернуть.
        - name: offset
          in: query
          type: integer
          required: false
          minimum: 0
          default: 0
          description: Отступ, с которым необходимо вернуть пользователей.
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    users:
                        type: array
                        required: true
                        description: Массив с информацией о запрашиваемых пользователях.
                        items:
                            type: object
                            properties:
                                user_id:
                                    type: integer
                                    description: Локальный идентификатор пользователя чата.
                                general_user_id:
                                    type: integer
                                    description: Основной идентификатор пользователя в системе (совпадает с
                                                 идентификатором в сервисе Profile).
                                is_online:
                                    type: boolean
                                    description: Флаг статуса онлайна (true - пользователь онлайн,
                                                 false - пользователь оффлайн).
                                is_banned:
                                    type: boolean
                                    description: Флаг статуса блокировки (true - пользователь заблокирован в чате,
                                                 false - пользователь не заблокирован).
                                ban_reason:
                                    type: string
                                    description: Причина блокировки.
                                ban_ts:
                                    type: integer
                                    description: Дата и время блокировки (в формате unix timestamp).
                                ban_expire_at:
                                    type: integer
                                    description: Дата и время снятия блокировки (в формате unix timestamp).
                            required:
                                - user_id
                                - general_user_id
                                - is_online
                                - is_banned
                required:
                    - users
    """
    pass


@check_permissions('group:moderator', 'group:admin')
@authorized('admin')
async def block_user(request: aiohttp.web.Request):
    """
    ---
    summary: Блокировать пользователя в общем чате на основном сайте
    description: Скрываются все сообщения пользователя в чате, он не имеет возможности писать в чат.
                 Пользователь должен обладать правами модератора и выше.
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, которого необходимо заблокировать в чате (локальный).
        - name: expire
          in: path
          type: integer
          required: false
          default: 0
          description: Время в секундах, на которое блокируется пользователь (0 - перманентная блокировка).
        - name: reason
          in: query
          type: string
          required: true
          description: Причина блокировки.
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
    responses:
        200:
            description: OK
    """
    pass


@check_permissions('group:moderator', 'group:admin')
@authorized('admin')
async def unblock_user(request: aiohttp.web.Request):
    """
    ---
    summary: Разблокировать пользователя в общем чате на основном сайте
    description: Старые сообщения пользователя не восстанавливаются.
                 Пользователь должен обладать правами модератора и выше.
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, которого необходимо разблокировать в чате (локальный).
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
    responses:
        200:
            description: OK
    """
    pass
