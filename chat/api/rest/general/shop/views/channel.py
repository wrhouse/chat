from datetime import datetime

import aiohttp.web
from sqlalchemy import insert, select

from chat.db.general.shop import MessageModel, ChannelModel
from chat.decorators import check_permissions, authorized
from chat.utils.channel import Channel


def get_channel_key(channel_id: int) -> str:
    return f'channel:{channel_id}'


async def get_channels(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка активных каналов в общем чате на основном сайте
    tags:
        - General Shop REST API
    produces:
        - application/json
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    channels:
                        type: array
                        description: Массив с информацией о каналах
                        items:
                            type: object
                            properties:
                                channel_id:
                                    type: integer
                                    description: Идентификатор канала
                                name:
                                    type: string
                                    description: Название канала
                                description:
                                    type: string
                                    description: Описание канала
                                    x-nullable: true
                                image:
                                    type: string
                                    description: Иконка канала в формате base64
                                    x-nullable: true
                            required:
                                - channel_id
                                - name
                required:
                    - channels
    """

    async with request.app['db'].acquire() as conn:
        select_query = select([ChannelModel])
        cursor = await conn.execute(select_query)
        channels = await cursor.fetchall()

        return {
            'channels': [ChannelModel(**channel).to_full_dict() for channel in channels]
        }


@check_permissions('group:admin')
@authorized('admin')
async def add_channel(request: aiohttp.web.Request):
    """
    ---
    summary: Добавление канала в общий чат на основном сайте
    description: Пользователь должен обладать правами администратора и выше.
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
        - name: data
          in: body
          required: true
          schema:
            type: object
            properties:
                name:
                    type: string
                    description: Название создаваемого канала.
                description:
                    type: string
                    description: Описание создаваемого канала.
                    x-nullable: true
                image:
                    type: string
                    description: Иконка канала в формате base64.
                    x-nullable: true
            required:
                - name
    responses:
        200:
            description: OK
    """
    pass


async def get_channel_history(request: aiohttp.web.Request):
    """
    ---
    summary: Получение истории сообщений в канале
    description: Идентификаторы пользователей являются локальными (для текущего сервиса).
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: channel_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор канала, для которого требуется получить историю сообщений.
        - name: start_message_id
          in: query
          type: integer
          required: false
          default: 0
          minimum: 0
          description: Если значение > 0, то это идентификатор сообщения, начиная с которого нужно вернуть
                       историю сообщений в канале, если передано значение 0 то вернутся сообщения с самого начала.
        - name: count
          in: query
          type: integer
          required: false
          default: 20
          minimum: 1
          maximum: 200
          description: Количество сообщений, которое необходимо получить (но не больше 200).
        - name: rev
          in: query
          type: integer
          required: false
          default: 0
          minimum: 0
          maximum: 1
          description: Флаг, отвечающий за порядок, в котором требуется вернуть сообщения (0 - в обратном
                       хронологическом порядке (по умолчанию), 1 - в хронологическом порядке).
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    messages:
                        type: array
                        description: Массив с сообщениями.
                        items:
                            type: object
                            properties:
                                message_id:
                                    type: integer
                                    description: Идентификатор сообщения.
                                from_id:
                                    type: integer
                                    description: Идентификатор пользователя, отправившего сообщение.
                                created_at:
                                    type: string
                                    description: Дата и время отправки сообщения (в формате 'YYYY-MM-DD hh:mm:ss').
                                text:
                                    type: string
                                    description: Текст сообщения
                            required:
                                - message_id
                                - from_id
                                - date
                                - text
                required:
                    - messages
    """

    channel_id = int(request.match_info['channel_id'])
    start_message_id = int(request.rel_url.query.get('start_message_id', 0))
    count = int(request.rel_url.query.get('count', 20))
    rev = int(request.rel_url.query.get('rev', 0))

    async with request.app['db'].acquire() as conn:
        select_query = select([MessageModel]).where(MessageModel.channel_id == channel_id)

        # Выбор сообщения, с которого требуется сформировать выдачу
        if start_message_id > 0:
            if rev == 0:
                select_query = select_query.where(MessageModel.message_id < start_message_id)
            elif rev == 1:
                select_query = select_query.where(MessageModel.message_id > start_message_id)

        # Конфигурирование порядка сообщений
        if rev == 0:
            select_query = select_query.order_by(MessageModel.message_id.desc())
        elif rev == 1:
            select_query = select_query.order_by(MessageModel.message_id.asc())

        # Ограничение количества сообщений в выдаче
        select_query = select_query.limit(count)

        cursor = await conn.execute(select_query)
        messages = await cursor.fetchall()

        return {
            'messages': [MessageModel(**message).to_minimalistic_dict() for message in messages]
        }


@check_permissions('permission:write_general_shop_chat')
@authorized('shop')
async def send_message_to_channel(request: aiohttp.web.Request):
    """
    ---
    summary: Отправка сообщения в канал
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: channel_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор канала, в который требуется отправить сообщение.
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Shop.
        - name: data
          in: body
          required: true
          schema:
            type: object
            properties:
                text:
                    type: string
                    minLength: 1
                    description: Текст сообщения.
            required:
                - text
    responses:
        200:
            description: OK
    """

    user_id = request['user_id']
    channel_id = int(request.match_info['channel_id'])
    request_data = await request.json()
    text = request_data['text']

    message = MessageModel(from_id=user_id,
                           channel_id=channel_id,
                           text=text,
                           created_at=datetime.utcnow())

    async with request.app['db'].acquire() as conn:
        insert_query = insert(MessageModel).returning(MessageModel.message_id).values({
            'from_id': message.from_id,
            'channel_id': message.channel_id,
            'text': message.text,
            'created_at': message.created_at
        })
        cursor = await conn.execute(insert_query)
        message.message_id, *_ = await cursor.fetchone()

    channel_key = get_channel_key(channel_id)
    chat_channels = request.app['channels']['general']['shop']
    if channel_key not in chat_channels:
        chat_channels[channel_key] = Channel(channel_key, request.app['redis'])
    await chat_channels[channel_key].publish_message(message.to_minimalistic_dict())


@check_permissions('group:admin')
@authorized('admin')
async def delete_channel(request: aiohttp.web.Request):
    """
    ---
    summary: Удаление канала из общего чата на основном сайте
    description: Пользователь должен обладать правами администратора и выше.
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: channel_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор канала, который требуется удалить.
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
    responses:
        200:
            description: OK
    """
    pass


@check_permissions('group:moderator', 'group:admin')
@authorized('admin')
async def delete_message_from_channel(request: aiohttp.web.Request):
    """
    ---
    summary: Удаление сообщения из канала в общем чате на основном сайте
    description: Пользователь должен обладать правами администратора и выше.
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: channel_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор канала, из которого требуется удалить сообщение.
        - name: message_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор сообщения в канале, которое требуетяс удалить.
        - name: access_token
          in: query
          type: string
          required: true
          description: Токен доступа пользователя к сервису Admin Panel.
    responses:
        200:
            description: OK
    """
    pass


async def search_messages_from_channel(request: aiohttp.web.Request):
    """
    ---
    summary: Полнотекстовый поиск сообщений в канале
    tags:
        - General Shop REST API
    produces:
        - application/json
    parameters:
        - name: channel_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор канала, в котором необходимо найти сообщения.
        - name: query
          in: query
          type: string
          required: true
          description: Поисковой запрос.
        - name: start_message_id
          in: query
          type: integer
          required: false
          default: 0
          minimum: 0
          description: Если значение > 0, то это идентификатор сообщения, начиная с которого нужно искать
                       сообщения в канале, если передано значение 0 то поиск начнется с самого начала.
        - name: count
          in: query
          type: integer
          required: false
          default: 20
          minimum: 1
          maximum: 200
          description: Количество идентификаторов сообщений, которые необходимо получить (но не больше 200).
        - name: rev
          in: query
          type: integer
          required: false
          default: 0
          minimum: 0
          maximum: 1
          description: Флаг, отвечающий за порядок, в котором требуется вернуть идентификаторы сообщений (0 - в обратном
                       хронологическом порядке (по умолчанию), 1 - в хронологическом порядке).
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    message_ids:
                        type: array
                        description: Массив идентификаторов сообщений, удовлетворяющих поисковому запросу.
                        items:
                            type: integer
                required:
                    - message_ids
    """
    pass
