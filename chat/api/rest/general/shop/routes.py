import aiohttp.web

from chat.api.rest.general.shop.views.channel import (
    get_channels,
    add_channel,
    get_channel_history,
    send_message_to_channel,
    delete_channel,
    delete_message_from_channel,
    search_messages_from_channel,
)
from chat.api.rest.general.shop.views.user import (
    get_users,
    block_user,
    unblock_user
)


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix + '/channel', get_channels)
    app.router.add_post(prefix + '/channel', add_channel)
    app.router.add_get(prefix + r'/channel/{channel_id:\d+}', get_channel_history)
    app.router.add_post(prefix + r'/channel/{channel_id:\d+}', send_message_to_channel)
    app.router.add_delete(prefix + r'/channel/{channel_id:\d+}', delete_channel)
    app.router.add_delete(prefix + r'/channel/{channel_id:\d+}/message/{message_id:\d+}', delete_message_from_channel)
    app.router.add_get(prefix + r'/channel/{channel_id:\d+}/search', search_messages_from_channel)

    app.router.add_get(prefix + '/user', get_users)
    app.router.add_delete(prefix + r'/user/{user_id:\d+}', block_user)
    app.router.add_put(prefix + r'/user/{user_id:\d+}', unblock_user)
