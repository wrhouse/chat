from functools import wraps
from typing import List

import aiohttp
import aiohttp.web
import jwt


# TODO: Сделать проверку прав и получение user_id по нормальному
SERVICE_SECRETS = {
    'profile': 'tE73SBK8MbGC2u9xYMtQVT72urSam5Su',
    'shop': 'SexUPiN6ujtpwNNFvjwNqT4rWovyPGZh',
    'forum': 'hMZy9cHEmoGUpjNmqpaQTDqpRWHeKowp'
}


def parse_jwt_token(jwt_token: str,
                    secret: str,
                    verify: bool = True) -> dict:
    jwt_data = jwt.decode(jwt_token,
                          secret,
                          leeway=10,
                          verify=verify,
                          algorithms=['HS256'])
    del jwt_data['created_at']
    del jwt_data['exp']
    return jwt_data


async def user_id_by_session_id(session_id: str) -> int:
    async with aiohttp.ClientSession() as session:
        async with session.get(f"http://tokenizer_app:80/api/session/{session_id}") as resp:
            resp_json = await resp.json()
            user_id = resp_json.get("data", {}).get("user_id")
            return user_id


def authorized(service_id: str):
    def decorator(handler):
        @wraps(handler)
        async def wrapper(request: aiohttp.web.Request):
            access_token = request.rel_url.query.get("access_token")
            session_id = parse_jwt_token(access_token, SERVICE_SECRETS[service_id])['session_id']
            try:
                request['user_id'] = await user_id_by_session_id(session_id)
            except:
                request['user_id'] = None
            return await handler(request)

        return wrapper

    return decorator


def check_permissions(*permissions: List[str]):
    def decorator(handler):
        @wraps(handler)
        async def wrapper(request: aiohttp.web.Application):
            return await handler(request)

        return wrapper
    return decorator
