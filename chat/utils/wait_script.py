import asyncio
import random
import socket
import time

import aioredis

from chat.settings import Settings

if __name__ == "__main__":
    settings = Settings()
    postgres_connected = "FAILED"
    redis_connected = "FAILED"
    while True:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                redis_address = f'redis://{settings.redis_host}:{settings.redis_port}'
                asyncio.get_event_loop().run_until_complete(
                    aioredis.create_connection(address=redis_address,
                                               db=settings.redis_database,
                                               password=settings.redis_password)
                )
                redis_connected = "  OK  "

                sock.connect((settings.database_host, settings.database_port))
                postgres_connected = "  OK  "

                print("[  OK  ] Postgresql connected")
                print("[  OK  ] Redis connected")
                break
        except socket.error:
            print(f"[{postgres_connected}] Postgresql connected")
            print(f"[{redis_connected}] Redis connected")
            time.sleep(0.5 + (random.randint(0, 100) / 1000))
