import asyncio
from typing import Union

import aiohttp.web
import ujson as json


class Channel:
    def __init__(self,
                 channel_key: str,
                 redis_connection):
        self._channel_key = channel_key
        self._redis_connection = redis_connection
        self._ws_connections = set()
        self._task = None
        self._is_running = False

    def add_connection(self,
                       ws: aiohttp.web.WebSocketResponse) -> bool:
        if ws in self._ws_connections:
            return False
        self._ws_connections.add(ws)
        return True

    def remove_connection(self,
                          ws: aiohttp.web.WebSocketResponse) -> bool:
        if ws not in self._ws_connections:
            return False
        self._ws_connections.remove(ws)
        return True

    def is_empty(self) -> bool:
        return not bool(self._ws_connections)

    def is_running(self) -> bool:
        return self._is_running

    def start(self) -> None:
        loop = asyncio.get_event_loop()
        self._task = loop.create_task(self.delivery_task())
        self._is_running = True

    async def stop(self) -> None:
        if self._task is not None:
            await self._redis_connection.unsubscribe(self._channel_key)
            self._task.cancel()
            self._task = None
            self._ws_connections.clear()
            self._is_running = False

    async def delivery_task(self) -> None:
        channel, *_ = await self._redis_connection.subscribe(self._channel_key)
        while await channel.wait_message():
            message = await channel.get(encoding='utf-8')
            tasks = [self.send_message(ws, message) for ws in self._ws_connections]
            await asyncio.wait(tasks)

    async def publish_message(self,
                              message: Union[dict, str]) -> bool:
        if type(message) == dict:
            message = json.dumps(message)
        try:
            await self._redis_connection.publish(self._channel_key, message)
            return True
        except:
            return False

    async def send_message(self,
                           ws: aiohttp.web.WebSocketResponse,
                           message: str) -> bool:
        try:
            await ws.send_str(message)
            return True
        except:
            self.remove_connection(ws)
            return False
