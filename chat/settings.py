import os
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    @property
    def service_host(self) -> str:
        return os.environ["SERVICE_HOST"]

    @property
    def service_port(self) -> int:
        return int(os.environ["SERVICE_PORT"])

    @property
    def environment(self) -> str:
        return os.environ["ENVIRONMENT"]

    @property
    def database_host(self) -> str:
        return os.environ["DATABASE_HOST"]

    @property
    def database_port(self) -> int:
        return int(os.environ["DATABASE_PORT"])

    @property
    def database_user(self) -> str:
        return os.environ["DATABASE_USER"]

    @property
    def database_password(self) -> str:
        return os.environ["DATABASE_PASSWORD"]

    @property
    def database_name(self) -> str:
        return os.environ["DATABASE_NAME"]

    @property
    def redis_host(self) -> str:
        return os.environ["REDIS_HOST"]

    @property
    def redis_port(self) -> int:
        return int(os.environ["REDIS_PORT"])

    @property
    def redis_password(self) -> Optional[str]:
        return os.environ.get("REDIS_PASSWORD") or None

    @property
    def redis_database(self) -> Optional[int]:
        redis_database = os.environ.get("REDIS_DATABASE")
        return int(redis_database) if redis_database else None
