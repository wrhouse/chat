import asyncio

import aiohttp.web
import aiohttp_debugtoolbar
import aioredis
import uvloop
from aiohttp_swagger import setup_swagger
from aiopg.sa import create_engine

from chat.middlewares import SERVICE_MIDDLEWARES
from chat.routes import init_routes
from chat.settings import Settings
from chat.utils.enum import Environment


async def database_connection(app: aiohttp.web.Application):
    settings = Settings()

    app['db'] = await create_engine(user=settings.database_user,
                                    password=settings.database_password,
                                    database=settings.database_name,
                                    host=settings.database_host,
                                    port=settings.database_port)

    yield

    app['db'].close()
    await app['db'].wait_closed()


async def redis_connection(app: aiohttp.web.Application):
    settings = Settings()

    redis_address = f'redis://{settings.redis_host}:{settings.redis_port}'
    app['redis'] = await aioredis.create_redis_pool(address=redis_address,
                                                    db=settings.redis_database,
                                                    password=settings.redis_password)

    yield

    app['redis'].close()
    await app['redis'].wait_closed()


def init_message_channels(app: aiohttp.web.Application):
    app['channels'] = {
        'general': {
            'forum': dict(),
            'shop': dict()
        },
        'order': {
            'shop': dict()
        },
        'support': dict()
    }


def setup_server(settings: Settings) -> aiohttp.web.Application:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    app = aiohttp.web.Application(middlewares=SERVICE_MIDDLEWARES)

    init_routes(app=app)
    init_message_channels(app=app)
    if Environment(settings.environment) != Environment.PRODUCTION:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)
        setup_swagger(app, swagger_url='/api/reference')

    app.cleanup_ctx.extend([database_connection, redis_connection])

    return app


def create_app():
    settings = Settings()
    app = setup_server(settings)
    return app


def main() -> None:
    settings = Settings()

    app = setup_server(settings)
    aiohttp.web.run_app(app, host=settings.service_host, port=settings.service_port)


if __name__ == '__main__':
    main()
