import traceback

from aiohttp.web import middleware

from chat.responses import APIResponse, APIException, InternalServerError


@middleware
async def cors_middleware(request, handler):
    response = await handler(request)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@middleware
async def rest_api_response_formatter(request, handler):
    response = await handler(request)
    if response is None or isinstance(response, dict):
        return APIResponse(data=response)
    return response


@middleware
async def internal_server_error_handler(request, handler):
    try:
        response = await handler(request)
        return response
    except APIException as api_exception:
        raise api_exception from None
    except Exception as msg:
        traceback.print_exc()
        raise InternalServerError()


SERVICE_MIDDLEWARES = [cors_middleware,
                       rest_api_response_formatter,
                       internal_server_error_handler]
