class DataMapperMixin:
    def to_minimalistic_dict(self) -> dict:
        raise NotImplementedError("Method 'to_minimalistic_dict' has not been implemented")

    def to_full_dict(self) -> dict:
        raise NotImplementedError("Method 'to_full_dict' has not been implemented")

