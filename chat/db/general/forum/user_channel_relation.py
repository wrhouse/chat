from sqlalchemy import Column, Integer, ForeignKey, Index
from sqlalchemy.sql import expression

from chat.db.general.forum.channel import ChannelModel
from chat.db.general.forum.user import UserModel
from chat.migrations import Base


class UserChannelRelationModel(Base):
    __tablename__ = 'general_forum_users_channels_relation'

    user_channel_relation_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(UserModel.user_id), nullable=False)
    channel_id = Column(Integer, ForeignKey(ChannelModel.channel_id), nullable=False)
    last_heartbeat_ts = Column(Integer, nullable=False, server_default=expression.literal(0))

    __table_args__ = (
        Index('general_forum_users_channels_relation_idx', channel_id, last_heartbeat_ts.asc()),
    )
