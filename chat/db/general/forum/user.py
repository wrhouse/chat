from sqlalchemy import Column, Integer, Boolean
from sqlalchemy.sql import expression

from chat.migrations import Base


class UserModel(Base):
    __tablename__ = 'general_forum_users'

    user_id = Column(Integer, primary_key=True)
    general_user_id = Column(Integer, nullable=False, unique=True)
    is_verified = Column(Boolean, nullable=False, server_default=expression.false())
    is_banned = Column(Boolean, nullable=False, server_default=expression.false())
