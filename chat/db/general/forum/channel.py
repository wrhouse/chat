from sqlalchemy import Column, Integer, String, Text

from chat.migrations import Base


class ChannelModel(Base):
    __tablename__ = 'general_forum_channels'

    channel_id = Column(Integer, primary_key=True)
    name = Column(String(length=128), nullable=False, unique=True)
    description = Column(Text, nullable=True)
    image = Column(Text, nullable=True)
