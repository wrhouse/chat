from sqlalchemy import Column, Integer, ForeignKey, Text, DateTime, Boolean, Index
from sqlalchemy.sql import func, expression

from chat.db.general.forum.channel import ChannelModel
from chat.db.general.forum.user import UserModel
from chat.migrations import Base


class MessageModel(Base):
    __tablename__ = 'general_forum_messages'

    message_id = Column(Integer, primary_key=True)
    from_id = Column(Integer, ForeignKey(UserModel.user_id), nullable=False)
    channel_id = Column(Integer, ForeignKey(ChannelModel.channel_id), nullable=False)
    text = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    is_hidden = Column(Boolean, nullable=False, server_default=expression.false())

    # Must be added to migration:
    # op.create_index('general_forum_messages_text_tsv', 'general_forum_messages', [sa.text('to_tsvector(\'english\'::regconfig, text)')], postgresql_using='gin')
    __table_args__ = (
        Index('general_forum_messages_text_tsv', func.to_tsvector('english', text), postgresql_using='gin'),
        Index('general_forum_messages_created_at_idx', created_at.asc()),
    )
