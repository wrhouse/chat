from sqlalchemy import Column, Integer, Index, DateTime, String
from sqlalchemy.sql import func

from chat.migrations import Base


class BanInfoModel(Base):
    __tablename__ = 'general_shop_ban_info'

    ban_info_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    expire_at = Column(DateTime, nullable=False)
    reason = Column(String, nullable=True)

    __table_args__ = (
        Index('general_shop_ban_info_expire_idx', user_id, expire_at),
    )
