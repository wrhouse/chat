from sqlalchemy import Column, Integer, String, Text

from chat.db.mixins import DataMapperMixin
from chat.migrations import Base


class ChannelModel(Base, DataMapperMixin):
    __tablename__ = 'general_shop_channels'

    channel_id = Column(Integer, primary_key=True)
    name = Column(String(length=128), nullable=False, unique=True)
    description = Column(Text, nullable=True)
    image = Column(Text, nullable=True)

    def to_minimalistic_dict(self) -> dict:
        return dict(channel_id=self.channel_id,
                    name=self.name)

    def to_full_dict(self) -> dict:
        return dict(channel_id=self.channel_id,
                    name=self.name,
                    description=self.description,
                    image=self.image)
