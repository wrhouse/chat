from sqlalchemy import Column, Integer, ForeignKey, Text, DateTime, Boolean, Index
from sqlalchemy.sql import func, expression

from chat.db.mixins import DataMapperMixin
from chat.db.general.shop.channel import ChannelModel
from chat.migrations import Base


class MessageModel(Base, DataMapperMixin):
    __tablename__ = 'general_shop_messages'

    message_id = Column(Integer, primary_key=True)
    from_id = Column(Integer, nullable=False)
    channel_id = Column(Integer, ForeignKey(ChannelModel.channel_id), nullable=False)
    text = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    is_hidden = Column(Boolean, nullable=False, server_default=expression.false())

    # Must be added to migration:
    # op.create_index('general_shop_messages_text_tsv', 'general_shop_messages', [sa.text('to_tsvector(\'english\'::regconfig, text)')], postgresql_using='gin')
    __table_args__ = (
        Index('general_shop_messages_text_tsv', func.to_tsvector('english', text), postgresql_using='gin'),
        Index('general_shop_messages_created_at_idx', created_at.asc()),
    )

    def to_minimalistic_dict(self) -> dict:
        return dict(message_id=self.message_id,
                    channel_id=self.channel_id,
                    from_id=self.from_id,
                    text=self.text,
                    created_at=self.created_at.strftime('%Y-%m-%d %H:%M:%S'))

    def to_full_dict(self) -> dict:
        return dict(message_id=self.message_id,
                    from_id=self.from_id,
                    channel_id=self.channel_id,
                    text=self.text,
                    created_at=self.created_at.strftime('%Y-%m-%d %H:%M:%S'),
                    is_hidden=self.is_hidden)
