FROM python:3.7-alpine

MAINTAINER v.bakaev <vlad@bakaev.tech>

WORKDIR /app

RUN apk add --no-cache g++ \
                       gcc \
                       bash \
                       make \
                       postgresql-dev \
                       linux-headers

RUN pip install --no-cache-dir cython

ARG requirements=requirements/production.txt
COPY requirements requirements
RUN pip install --no-cache-dir -r $requirements

ADD . /app
RUN pip install --no-cache-dir -e .
